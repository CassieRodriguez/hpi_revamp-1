#!/usr/bin/python
#Scripts covering line 1229 to 1288 of HPI_monthly.sh
import ConfigParser
import MySQLdb
import mysql.connector
import pandas as pd
import logging
import os
from datetime import datetime
import dateutil.relativedelta
check_table_existance = __import__('1_monthly_hpi').check_table_existance

#############################################################################################################
# Script has one function:                                                                                  #
#      -final_monthly_hpi  :   creating 'hpi_month_change' table and calculating diff_cs                    #
#                              diff_fhfa, diff_sz, diff_final using 'cs_[curr_month]_cbsas_sa_horizontal',  #
#                              'hpi_month_fhfa_state_division_[curr_month]_horizontal',                     #
#                              'hpi_year_sz_tract_[run_date]' tables                                        #
#                                                                                                           #
#   curr_month      :   201802      -   Current year and month when delivery happens                        #
#   run_date        :   20180130    -   Latest DPS date                                                     #
#   last_dps_ym     :   201712      -   Last DPS year and month                                             #
#   last3_ym        :   201711      -   year and second previous month from rundate                         #
#   last4_ym        :   201710      -   year and third previous month from rundate                          #
#   last_dps_year   :   2017        -   year of previous month from rundate (201712)                        #
#   last3month_year :   2017        -   year of previous month from rundate (201711)                        #
#############################################################################################################


def final_monthly_hpi():
    logging.basicConfig(filename='hpi_logs.log', level=logging.DEBUG, format='%(asctime)s %(levelname)s : %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.info("Started Executing '4_final_monthly_hpi.py' script")
    configparser = ConfigParser.RawConfigParser()
    config_filepath = r'../docs/credentials.cfg'
    configparser.read(config_filepath)

    user = configparser.get('aws2-config', 'user')
    pswd = configparser.get('aws2-config', 'pswd')
    host = configparser.get('aws2-config', 'hostdps')
    databs = configparser.get('aws2-config', 'databs')
    databs_hpis = configparser.get('aws2-config', 'databs_hpis')

    db = MySQLdb.connect(host=host, user=user, passwd=pswd)
    cursor = db.cursor()
    logging.captureWarnings(cursor)

    cnx = mysql.connector.connect(host=host, user=user, passwd=pswd)
    cursor_cnx = cnx.cursor()
    logging.captureWarnings(cursor_cnx)

    # Date manipulations
    now = datetime.now()
    curr_month = now.strftime('%Y%m')
    run_datetime = now + dateutil.relativedelta.relativedelta(months=-1)
    run_date = run_datetime.strftime('%Y%m30')
    last_dps_datetime = now + dateutil.relativedelta.relativedelta(months=-2)
    last_dps_date = last_dps_datetime.strftime('%Y%m30')
    last_dps_ym = last_dps_datetime.strftime('%Y%m')
    last3month_datetime = now + dateutil.relativedelta.relativedelta(months=-3)
    last3_ym = last3month_datetime.strftime('%Y%m')
    last4month_datetime = now + dateutil.relativedelta.relativedelta(months=-4)
    last4_ym = last4month_datetime.strftime('%Y%m')
    last_dps_year = last_dps_datetime.strftime('%Y')
    last3month_year = last3month_datetime.strftime('%Y')

    check_table_existance("geo_tracts", "geo_data", cursor, logging)
    df_hpi_month_change = pd.read_sql("""SELECT id tract_geoid, geo_cbsa_id cbsa_geoid, geo_county_id county_geoid, geo_state_id state_fips
                FROM geo_data.geo_tracts;""", con=db)
    logging.info("Fetched from geo_data")

    check_table_existance("cs_" + curr_month + "_cbsas_sa_horizontal", databs, cursor, logging)
    df_cbsas_sa_horizontal = pd.read_sql("SELECT cbsa_geoid, val_" + last3_ym + ", val_" + last4_ym + " FROM " + databs + ".cs_" + curr_month + "_cbsas_sa_horizontal;", con=db)
    logging.info("Fetched from cbsas_sa_horizontal")
    # Replacing NONE values with 0 in the pandas
    df_cbsas_sa_horizontal.loc[df_cbsas_sa_horizontal['val_' + last3_ym].isnull(), 'val_' + last3_ym] = 0

    # Adding column for column A / column B division based on date
    df_cbsas_sa_horizontal['diff_cs'] = (df_cbsas_sa_horizontal['val_' + last3_ym]/df_cbsas_sa_horizontal['val_' + last4_ym])
    df_cbsas_sa_horizontal.drop(['val_' + last3_ym, 'val_' + last4_ym], axis=1, inplace=True)
    merged_df_lev_1 = pd.merge(df_hpi_month_change, df_cbsas_sa_horizontal, on="cbsa_geoid", how="left")

    check_table_existance("hpi_month_fhfa_state_division_" + curr_month + "_horizontal", databs, cursor, logging)
    df_month_fhfa_state_division = pd.read_sql("SELECT state_fips, val_" + last3_ym+", val_" + last4_ym+" FROM " + databs + ".hpi_month_fhfa_state_division_" + curr_month + "_horizontal;", con=db)
    logging.info("Fetched from hpi_month_fhfa_state_division")

    # Replacing NONE values with 0 in the pandas
    df_month_fhfa_state_division.loc[df_month_fhfa_state_division['val_' + last3_ym].isnull(), 'val_' + last3_ym] = 0
    df_month_fhfa_state_division['diff_fhfa'] = df_month_fhfa_state_division['val_' + last3_ym]/df_month_fhfa_state_division['val_' + last4_ym]
    df_month_fhfa_state_division.drop(['val_' + last3_ym, 'val_' + last4_ym], axis=1, inplace=True)
    merged_df_lev_2 = pd.merge(merged_df_lev_1, df_month_fhfa_state_division, on="state_fips", how="left")

    check_table_existance("hpi_year_sz_tract_" + run_date, databs_hpis, cursor, logging)
    df_hpi_prev_mon_sz_tract = pd.read_sql(
                "SELECT tract_geoid, val_" + last_dps_year + " as c_year FROM " + databs_hpis + ".hpi_year_sz_tract_" + run_date,
                con=db)
    logging.info("Fetched from hpi_year_sz_tract" + run_date)

    check_table_existance("hpi_year_sz_tract_" + last_dps_date, databs_hpis, cursor, logging)
    df_hpi_prev2_mon_sz_tract = pd.read_sql(
                "SELECT tract_geoid, val_" + last3month_year + " as l_year FROM " + databs_hpis + ".hpi_year_sz_tract_" + last_dps_date,
                con=db)
    logging.info("Fetched from hpi_year_sz_tract_" + last_dps_date)

    df_hpi_year_sz_tract = pd.merge(df_hpi_prev_mon_sz_tract, df_hpi_prev2_mon_sz_tract, on="tract_geoid", how="inner")
    df_hpi_year_sz_tract['diff_sz'] = df_hpi_year_sz_tract['c_year']/df_hpi_year_sz_tract['l_year']
    df_hpi_year_sz_tract.drop(['c_year', 'l_year'], axis=1, inplace=True)
    merged_df_lev_3 = pd.merge(merged_df_lev_2, df_hpi_year_sz_tract.drop_duplicates(subset=['tract_geoid']), on="tract_geoid", how="left")

    cols = ['tract_geoid', 'cbsa_geoid', 'county_geoid', 'state_fips', 'diff_cs', 'diff_fhfa', 'diff_sz']
    final_df = pd.DataFrame(merged_df_lev_3, columns=cols)

    final_df.to_csv("hpi_month_change.csv", encoding='utf-8', index=False)
    logging.info("CSV file 'hpi_month_change.csv' created")

    drop_table = "DROP TABLE IF EXISTS " + databs_hpis + ".hpi_month_change_" + run_date + ";"
    cursor.execute(drop_table)

    create_mon_chng = ("""CREATE TABLE """ + databs_hpis + """.hpi_month_change_""" + run_date + """
                (
                    tract_geoid bigint(11) NOT NULL,
                    cbsa_geoid mediumint(8) DEFAULT NULL,
                    county_geoid mediumint(8) DEFAULT NULL,
                    state_fips int(3) DEFAULT NULL,
                    diff_cs DOUBLE DEFAULT NULL,
                    diff_fhfa DOUBLE DEFAULT NULL,
                    diff_sz DOUBLE DEFAULT NULL,
                    diff_final DOUBLE DEFAULT NULL
                );""")
    cursor.execute(create_mon_chng)
    db.commit()
    logging.info("Table 'hpi_month_change_" + run_date + "' created '" + databs_hpis + "' in Database")

    load_mon_chng = ("""LOAD DATA LOCAL INFILE
                'hpi_month_change.csv'
                INTO TABLE
                """ + databs_hpis + ".hpi_month_change_" + run_date + """
                CHARACTER SET utf8
                FIELDS TERMINATED BY ','
                LINES TERMINATED BY '\n'
                IGNORE 1 LINES
                (tract_geoid, @vcbsa_geoid, county_geoid, state_fips, @vdiff_cs, @vdiff_fhfa, @vdiff_sz)
                SET
                cbsa_geoid = nullif(@vcbsa_geoid,''),
                diff_cs = nullif(@vdiff_cs,''),
                diff_fhfa = nullif(@vdiff_fhfa,''),
                diff_sz = nullif(@vdiff_sz,'')
                ;""")
    cursor.execute(load_mon_chng)
    logging.info("Load from CSV file 'hpi_month_change.csv' to Table 'hpi_month_change_" + run_date + "' completed")
    db.commit()

    sql_all = ("""SET @uplim =(SELECT ROUND(MAX(diff_cs),2) FROM """ + databs_hpis + """.hpi_month_change_""" + run_date + """);
    
    SET @lowlim =(SELECT TRUNCATE(MIN(diff_cs),2) FROM """ + databs_hpis + """.hpi_month_change_""" + run_date + """);
    
    UPDATE """ + databs_hpis + """.hpi_month_change_""" + run_date + """ SET diff_sz=@uplim WHERE diff_sz>@uplim;
    
    UPDATE """ + databs_hpis + """.hpi_month_change_""" + run_date + """ SET diff_sz=@lowlim WHERE diff_sz<@lowlim;
    
    UPDATE """ + databs_hpis + """.hpi_month_change_""" + run_date + """ SET diff_final=(diff_cs+diff_fhfa+diff_sz)/3 WHERE diff_cs IS NOT NULL AND diff_fhfa IS NOT NULL AND diff_sz IS NOT NULL;
    
    UPDATE """ + databs_hpis + """.hpi_month_change_""" + run_date + """ SET diff_final=(diff_cs+diff_sz)/2 WHERE diff_final IS NULL AND diff_cs IS NOT NULL AND diff_sz IS NOT NULL;
    
    UPDATE """ + databs_hpis + """.hpi_month_change_""" + run_date + """ SET diff_final=(diff_fhfa+diff_sz)/2 WHERE diff_final IS NULL AND diff_fhfa IS NOT NULL AND diff_sz IS NOT NULL;
    
    UPDATE """ + databs_hpis + """.hpi_month_change_""" + run_date + """ SET diff_final=(diff_cs+diff_fhfa)/2 WHERE diff_final IS NULL AND diff_cs IS NOT NULL AND diff_fhfa IS NOT NULL;
    
    UPDATE """ + databs_hpis + """.hpi_month_change_""" + run_date + """ SET diff_final=diff_cs WHERE diff_final IS NULL AND diff_cs IS NOT NULL;
    
    UPDATE """ + databs_hpis + """.hpi_month_change_""" + run_date + """ SET diff_final=diff_sz WHERE diff_final IS NULL AND diff_sz IS NOT NULL;
    
    UPDATE """ + databs_hpis + """.hpi_month_change_""" + run_date + """ SET diff_final=diff_fhfa WHERE diff_final IS NULL AND diff_fhfa IS NOT NULL;
    """)

    for _ in cursor_cnx.execute(sql_all, multi=True):
        pass

    cnx.commit()
    logging.info("Table 'hpi_month_change_" + run_date + "' update completed")
    cnx.close()

    os.remove("hpi_month_change.csv")
    logging.info("Removed intermediate CSV files")

    db2 = MySQLdb.connect(host=host, user=user, passwd=pswd)
    cursor2 = db2.cursor()
    logging.captureWarnings(cursor2)

    # Creating BackUp Table for 'szhpi_monthly_history'
    bkp_timestamp = datetime.now().strftime('%Y%m')
    create_mon_bkp = """CREATE TABLE IF NOT EXISTS """ + databs_hpis + """.szhpi_monthly_history_bkp_""" + bkp_timestamp + """
                SELECT * FROM """ + databs_hpis + """.szhpi_monthly_history;"""
    cursor2.execute(create_mon_bkp)
    db2.commit()
    logging.info("Backup Table 'szhpi_monthly_history_bkp_" + bkp_timestamp + "' for 'szhpi_monthly_history' created")
    
    # Check if col not exist or if it does,
    check_cols = "SELECT count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'szhpi_monthly_history' AND table_schema = 'hpis' AND column_name = 'val_"+last_dps_ym+"' ;"
    cursor2.execute(check_cols)
    (count_of_rows,) = cursor2.fetchone()
    if count_of_rows == 0:
    # Alter table, add column before update.
        alter_monthly_hist = "ALTER TABLE " + databs_hpis + ".szhpi_monthly_history ADD val_" + last_dps_ym +" decimal(8,4);"
        cursor2.execute(alter_monthly_hist)
    # Updating 'szhpi_monthly_history'
    update_mon_hist1 = "UPDATE " + databs_hpis + ".szhpi_monthly_history SET val_" + last_dps_ym + "=NULL;"
    cursor2.execute(update_mon_hist1)
    db2.commit()
    update_mon_hist2 = """UPDATE """ + databs_hpis + """.szhpi_monthly_history t, """ + databs_hpis + """.hpi_month_change_""" + run_date + """ s
                SET t.val_""" + last_dps_ym + """= ROUND(t.val_""" + last3_ym + """*s.diff_final, 4)
                WHERE t.tract_geoid=s.tract_geoid;"""
    cursor2.execute(update_mon_hist2)
    db2.commit()
    logging.info("Updated Table 'szhpi_monthly_history' in '" + databs_hpis + "' Database")

    logging.info("Verification of output tables started")
    # Verifying table 'hpi_month_change_[run_date]' data
    check_null = """SELECT count(*) FROM """ + databs_hpis + """.hpi_month_change_""" + run_date + """
                    WHERE tract_geoid IS NULL OR
                    county_geoid IS NULL OR
                    state_fips IS NULL OR
                    tract_geoid = '' OR
                    cbsa_geoid = '' OR
                    county_geoid = '' OR
                    state_fips = '' OR
                    diff_cs = '' OR
                    diff_fhfa = '' OR
                    diff_sz = '' OR
                    diff_final = '';"""
    logging.info("Test Case #1 : NULL or Empty values check")
    cursor.execute(check_null)
    if cursor.fetchone()[0] != 0:
        raise Exception("Null or Empty value exists")
    else:
        logging.info("Test Case #1 : Passed")

    check_tract = """SELECT count(*) FROM """ + databs_hpis + """.hpi_month_change_""" + run_date + """
                    WHERE tract_geoid NOT REGEXP '[0-9]' OR tract_geoid REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #2 : 'tract_geoid' values check")
    cursor.execute(check_tract)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid 'tract_geoid' value exists")
    else:
        logging.info("Test Case #2 : Passed")

    check_cbsa = """SELECT count(*) FROM """ + databs_hpis + """.hpi_month_change_""" + run_date + """
                    WHERE cbsa_geoid NOT REGEXP '[0-9]' OR cbsa_geoid REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #3 : 'cbsa_geoid' values check")
    cursor.execute(check_cbsa)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid 'cbsa_geoid' value exists")
    else:
        logging.info("Test Case #3 : Passed")

    check_county = """SELECT count(*) FROM """ + databs_hpis + """.hpi_month_change_""" + run_date + """
                    WHERE county_geoid NOT REGEXP '[0-9]' OR county_geoid REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #4 : 'county_geoid' values check")
    cursor.execute(check_county)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid 'county_geoid' value exists")
    else:
        logging.info("Test Case #4 : Passed")

    check_sfips = """SELECT count(*) FROM """ + databs_hpis + """.hpi_month_change_""" + run_date + """
                    WHERE state_fips NOT IN (select distinct(geo_state_id) from geo_data.geo_tracts);"""
    logging.info("Test Case #5 : 'state_fips' values check")
    cursor.execute(check_sfips)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid state_fips value exists")
    else:
        logging.info("Test Case #5 : Passed")

    check_diff = """SELECT count(*) FROM """ + databs_hpis + """.hpi_month_change_""" + run_date + """
                    WHERE diff_cs NOT BETWEEN 0 AND 1.5 OR
                    diff_cs REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]' OR
                    diff_fhfa NOT BETWEEN 0 AND 1.5 OR
                    diff_fhfa REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]' OR
                    diff_sz NOT BETWEEN 0 AND 1.5 OR
                    diff_sz REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]' OR
                    diff_final NOT BETWEEN 0 AND 1.5 OR
                    diff_final REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #6 : 'diff_cs', 'diff_fhfa', 'diff_sz', 'diff_final' values check")
    cursor.execute(check_diff)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid value exists in either 'diff_cs' , 'diff_fhfa', 'diff_sz' or 'diff_final'")
    else:
        logging.info("Test Case #6 : Passed")

    logging.info("Verification of output tables completed")
    db.close()

    logging.info("Completed Execution of '4_final_monthly_hpi.py' script")
