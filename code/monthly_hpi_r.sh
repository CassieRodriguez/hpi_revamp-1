#!/bin/bash -e
DBCON1="R --vanilla"
source "/mnt/tmp/mysql_conf/dps100.src"

DBCON2="mysql -h$host -u$user -p$password  --skip-column-names "
INIT_DATE=$(date -d "$date -1 month"  +%Y%m01)
yyyymm_inputs=$(date -d "${INIT_DATE} +1 month"  +%Y%m) #201610  ##current year month when delivery happens
yyyymmdd=$(date -d "${INIT_DATE} "  +%Y%m)30    #20160930 ## latest dps date
yyyymmdd_last=$(date -d "${INIT_DATE} -1 month"  +%Y%m)30 #20160830 ## last dps date
hpi_cur_month=$(date -d "${INIT_DATE} -1 month"  +%Y%m) #201608 ## the month of hpi being generated, which is often 2 months before yyyymm_inputs
hpi_last_month=$(date -d "${INIT_DATE} -2 month"  +%Y%m) #201607 ## the previous month of hpi generated
hpi_cur_month_cs=$(date -d "${INIT_DATE} -2 month"  +%Y%m) #201607 ## the month of CS/FHFA HPI, which is often 3 months before yyyymm_inputs
hpi_last_month_cs=$(date -d "${INIT_DATE} -3 month"  +%Y%m) #201606 ## the previous month of  CS/FHFA HPI

hpi_cur_year=${hpi_cur_month:0:4}
hpi_last_year=${hpi_last_month:0:4}

FHFA_CS_DB=hpi_fhfa_cs
HPI_DB=hpis

function monthly_qa_plots {

$DBCON1 << EOF
rm(list=ls(all=TRUE))
library(DBI)
library(class)
library("RMySQL")

if (${hpi_cur_year}==2012 ){
        xaxis <- c(200001:200012,200101:200112,200201:200212,200301:200312,200401:200412,200501:200512,200601:200612,
           200701:200712,200801:200812,200901:200912,201001:201012,201101:201112,201201:${hpi_cur_month})
} else if (${hpi_cur_year}==2013 ){
        xaxis <- c(200001:200012,200101:200112,200201:200212,200301:200312,200401:200412,200501:200512,200601:200612,
           200701:200712,200801:200812,200901:200912,201001:201012,201101:201112,201201:201212,
           201301:${hpi_cur_month})
} else if (${hpi_cur_year}==2014 ){
        xaxis <- c(200001:200012,200101:200112,200201:200212,200301:200312,200401:200412,200501:200512,200601:200612,
           200701:200712,200801:200812,200901:200912,201001:201012,201101:201112,201201:201212,
           201301:201312, 201401:${hpi_cur_month})
} else if (${hpi_cur_year}==2015 ){
        xaxis <- c(200001:200012,200101:200112,200201:200212,200301:200312,200401:200412,200501:200512,200601:200612,
           200701:200712,200801:200812,200901:200912,201001:201012,201101:201112,201201:201212,
           201301:201312, 201401:201412, 201501:${hpi_cur_month})
} else if (${hpi_cur_year}==2016 ){
        xaxis <- c(200001:200012,200101:200112,200201:200212,200301:200312,200401:200412,200501:200512,200601:200612,
           200701:200712,200801:200812,200901:200912,201001:201012,201101:201112,201201:201212,
           201301:201312, 201401:201412,201501:201512, 201601:${hpi_cur_month})
} else if (${hpi_cur_year}==2017 ){
        xaxis <- c(200001:200012,200101:200112,200201:200212,200301:200312,200401:200412,200501:200512,200601:200612,
           200701:200712,200801:200812,200901:200912,201001:201012,201101:201112,201201:201212,
           201301:201312, 201401:201412,201501:201512,201601:201612, 201701:${hpi_cur_month})
} else if (${hpi_cur_year}==2018 ){
        xaxis <- c(200001:200012,200101:200112,200201:200212,200301:200312,200401:200412,200501:200512,200601:200612,
           200701:200712,200801:200812, 200901:200912, 201001:201012, 201101:201112, 201201:201212,
           201301:201312, 201401:201412, 201501:201512, 201601:201612, 201701:201712, 201801:${hpi_cur_month})
} else {
    print("Wrong year. xaxis failed to create")
}


month_ct <- length(xaxis)

m <- dbDriver("MySQL")
con <- dbConnect(m, host="${host}", username="${user}", password="${password}", dbname="")
if(FALSE) {
Query2 = "select * from ${FHFA_CS_DB}.cs_${yyyymm_inputs}_cbsas_sa_horizontal ;"
cs <- dbGetQuery(con, Query2)

Query1 = "select * from ${HPI_DB}.szhpi_monthly_history;"

df = dbGetQuery(con, Query1)


pdf(paste("hpi_matplot_by_cbsa_",${yyyymmdd},".pdf",sep=""))

for (metro in unique(cs\$cbsa_geoid)){


df2 <-  df[df\$cbsa_geoid==metro,17:(16+month_ct)]

cs2 <-  cs[cs\$cbsa_geoid==metro,c(2:month_ct+1,(ncol(cs)-1),ncol(cs))]

matplot(t(df2),type="l",col="grey", xaxt = "n", ylab="HPI",main=paste("SmartZip tract level HPI (",cs2[1,ncol(cs2)-1],", ",cs2[1,ncol(cs2)]," )",sep="") )
lines(t(cs2),  col="red",lwd=2)
axis(1, at= 1:length(xaxis),labels = xaxis, cex.axis = 0.7, tick=F)
legend(x="topleft",pch=1,c("SmartZip tract HPIs","Case Shiller HPI"), col=c("grey","red"),cex=0.6)
}
dev.off()
}
#======================================== state =====================================

con <- dbConnect(m, host="${host}", username="${user}", password="${password}", dbname="")

Query2 = "select * from ${FHFA_CS_DB}.hpi_month_fhfa_state_division_${yyyymm_inputs}_horizontal ;"
fhfa <- dbGetQuery(con, Query2)
print (fhfa[,2])
for (i in 3:(ncol(fhfa)-2)) {
fhfa[,i] <- 100*fhfa[,i]/fhfa[,3]
}

fhfa[,2] <- 100

pdf(paste("hpi_matplot_by_state_",${yyyymmdd},".pdf",sep=""))

for (state in unique(fhfa\$state_fips)){
  fhfa2 <-  fhfa[fhfa\$state_fips==state,]
  matplot(t(df2),type="l",col="grey", xaxt = "n", ylab="HPI",main=paste("SmartZip tract level HPI (",fhfa2[1,ncol(fhfa2)]," )",sep="") )
  lines(t(fhfa2[2:(month_ct+1)]),  col="red",lwd=2)
  axis(1, at= 1:length(xaxis),labels = xaxis, cex.axis = 0.7, tick=F)
  legend(x="topleft",pch=1,c("SmartZip tract HPIs","FHFA HPI (Census Division)"), col=c("grey","red"),cex=0.6)
}

dev.off()

EOF
}



function annual_qa_plots {

$DBCON1 << EOF

rm(list=ls(all=TRUE))


library(DBI)
library(class)
library("RMySQL")

m <- dbDriver("MySQL")
con <- dbConnect(m, host="${host}", username="${user}", password="${password}", dbname="")

Query1 = "select * from ${HPI_DB}.szhpi_annual_history;"
df = dbGetQuery(con, Query1)

Query2 = "select * from ${FHFA_CS_DB}.hpi_year_cs_cbsa_${yyyymm_inputs};"
cs <- dbGetQuery(con, Query2)

Query3 ="select * from ${FHFA_CS_DB}.hpi_year_fhfa_cbsa_${yyyymm_inputs};"
fhfa_cbsa <- dbGetQuery(con, Query3)

Query4 = "select * from ${FHFA_CS_DB}.hpi_year_fhfa_state_${yyyymm_inputs};"
fhfa_state <- dbGetQuery(con, Query4)

xaxis <- 1990:${hpi_cur_year}
year_ct <- length(xaxis)

cbsa_avg <- data.frame(aggregate(df[,-(1:4)], list(df\$cbsa_geoid),mean))
colnames(cbsa_avg)[1]  <- "cbsa_geoid"
state_avg <- data.frame(aggregate(df[,-(1:4)], list(df\$state_fips),mean))
colnames(state_avg)[1]  <- "state_fips"

group.matplot.function <- function(vendor_name, vendor_data, sz_data, sz_data_name,vendor_data_name,geoid,a,b,c,d,sz_color){
  pdf(paste("hpi_matplot_year_",vendor_name,"_",${yyyymmdd},".pdf",sep=""))
    for (geo_id in unique(vendor_data\$geoid)){
    sz2 <-  sz_data[sz_data[[geoid]]==geo_id & !is.na(sz_data[[geoid]]),a:(b+year_ct)]
    vendor2 <-  vendor_data[vendor_data\$geoid==geo_id,c:(d+year_ct)]
    if(!is.na(sz2[1,1])) {
        matplot(t(sz2),type="l",col=sz_color, xaxt = "n", ylab="HPI",main=paste("SmartZip tract level HPI (",vendor2[1,1]," )",sep="") )
        lines(t(vendor2[,-1]),  col="red",lwd=2)
        axis(1, at= 1:length(xaxis),labels = xaxis, cex.axis = 0.7, tick=F)
        legend(x="topleft",pch=1,c(sz_data_name,vendor_data_name), col=c("grey","red"),cex=0.6)
    }
  }
  dev.off()
}

group.matplot.function("cs_cbsa",cs,df,"SmartZip tract HPIs","Case Shiller Metro HPI","cbsa_geoid",5,4,2,2,"grey")
group.matplot.function("fhfa_cbsa",fhfa_cbsa,df,"SmartZip tract HPIs","FHFA Metro HPI","cbsa_geoid",5,4,2,2,"grey")
group.matplot.function("fhfa_state",fhfa_state,df,"SmartZip tract HPIs","FHFA State HPI","state_fips",5,4,2,2,"grey")
group.matplot.function("cs_cbsa_avg",cs,cbsa_avg,"SmartZip Metro HPI (Tract Avg)","Case Shiller Metro HPI","cbsa_geoid",2,1,2,2,"blue")
group.matplot.function("fhfa_cbsa_avg",fhfa_cbsa,cbsa_avg,"SmartZip Metro HPI (Tract Avg)","FHFA Metro HPI","cbsa_geoid",2,1,2,2,"blue")
group.matplot.function("fhfa_state_avg",fhfa_state,state_avg,"SmartZip State HPI (Tract Avg)","FHFA State HPI","state_fips",2,1,2,2,"blue")


EOF
}

monthly_qa_plots
echo "monthly qa plots done"

annual_qa_plots
echo "annual qa plots done"

db=hpis
table=szhpi_annual_history
aws s3 rm  s3://smartzip-dps/data-lake/hpi/${table}/current/ --recursive
${DBCON2} << EOF1 > /mnt/tmp/${table}
SELECT * FROM $db.${table} ;
EOF1
aws s3 cp /mnt/tmp/${table} s3://smartzip-dps/data-lake/hpi/${table}/current/part_data
rm /mnt/tmp/${table}